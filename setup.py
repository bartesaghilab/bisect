#! /usr/bin/env python
from setuptools import setup
import BISECTomo
setup(
    name='BISECTomo',
    version=BISECTomo.__version__,
    description='Tomo module for Multiple Tomography with beam/image-shit',
    author='Jonathan Bouvette',
    author_email='jonathan.bouvette@nih.gov',
    packages=['BISECTomo', ],  # same as name
    install_requires=['numpy==1.20.2', 'pandas==1.0.3', 'matplotlib==3.3.1', 'scipy', 'psutil'],  # external packages as dependencies
    scripts=[
        'BISECTomo/cleanup.py',
        'BISECTomo/bisectomo.py',
    ],
)
