#!/usr/bin/env python

# BSD 3-Clause License

# Copyright (c) 2021, Bartesaghi lab (Duke University) and Borgnia lab (NIEHS/NIH)
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from BISECTomo.tomo_methods import *
import BISECTomo.config as config
import argparse
import os
import sys
import json
from multiprocessing import Pipe, Process, current_process
import time
import subprocess as sub
import psutil


class Settings(object):
    def __init__(self, adict):
        self.__dict__.update(adict)

    def update(self, newdata):
        for key, value in newdata.items():
            if not key in self.__dict__:
                print(f'\tAdding settings with {key}: {value}')
                setattr(self, key, value)
            else:
                if not value is None:
                    if not self.__dict__[key] == value:
                        print(f'\tUpdating settings with {key}: {value}')
                        setattr(self, key, value)

    def add_navItem(self, navIdict):
        if not 'NavItems' in self.__dict__:
            self.NavItems = dict()
        self.NavItems[navIdict['NavPosition']] = navIdict

    def save(self, filename):
        with open(filename, 'w') as f:
            json.dump(self.__dict__, f, indent=2)


def create_navItems(Nav, outputLoc, tilt_scheme):
    NavItems = []
    if type(Nav.latticeSize) == int:
        if not os.path.isfile(os.path.join(Nav.rootFolder, 'refined_global_coord.txt')):
            Nav.holeLattice()
        else:
            Nav.parse_refined_navItems()
        for item in Nav.navItems:
            if not 'NavItems' in args.__dict__ or not item in args.NavItems.keys():
                navIdict = {'rootfolder': Nav.rootFolder,
                            'NavPosition': item,
                            'tilt_scheme': tilt_scheme,
                            'ISareas': nav.ISareas,
                            'Status': None}
                args.add_navItem(navIdict)
            else:
                navIdict = args.NavItems[item]
            if navIdict['Status'] != 'Finished' and navIdict['Status'] != 'Completed':
                NavItems.append(NavItem(outfolder=outputLoc, **navIdict))

    else:
        for item in Nav.navItems:
            if os.path.isfile(os.path.join(Nav.rootFolder, item, 'refined_coord.txt')):
                Nav.parse_refined_navItems()
            if not 'NavItems' in args.__dict__ or not item in args.NavItems.keys():
                navIdict = {'rootfolder': Nav.rootFolder,
                            'NavPosition': item,
                            'tilt_scheme': tilt_scheme,
                            'ISareas': Nav.navItems[item]['Areas'],
                            'Status': None}
                args.add_navItem(navIdict)
            else:
                navIdict = args.NavItems[item]
            if navIdict['Status'] != 'Finished' and navIdict['Status'] != 'Completed':
                NavItems.append(NavItem(outfolder=outputLoc, **navIdict))
    return NavItems


def process_daemon(positions, conn):
    pid = os.getpid()
    stdout, stderr = os.path.join(args.outputLoc, 'logs', f'{current_process().name}_{str(pid)}.out'), os.path.join(
        args.outputLoc, 'logs', f'{current_process().name}_{str(pid)}.err')
    print(f"Starting {current_process().name} with pid:{pid}")  # \nRedirecting stdout and stderr to \n\t{stdout}\n\t{stderr}
    sys.stdout = open(stdout, "a")
    sys.stderr = open(stderr, "a")

    while positions[-1].finished is False:
        for pos in positions:
            pos.logic()
        time.sleep(5)
    else:
        print('Finished')
        conn.send(True)
        conn.close()


def write_BISECT_script(location, drive):
    with open(os.path.join(cfg._TEMPLATE_FILES, 'BISECT.txt'), 'r') as f:
        script = f.read()
    with open(os.path.join(location, 'BISECT.txt.lock'), 'w') as f:
        f.write(script.format(location=drive + location[len(cfg._WINDOWS_MOUNT_LOC[drive]) - 1:].replace('/', '\\')))
    shutil.move(os.path.join(location, 'BISECT.txt.lock'), os.path.join(location, 'BISECT.txt'))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Start the Tomography pipeline')
    parser.add_argument('--navigatorPath', '-nav', default=None, help='Full path to the navigator file')
    parser.add_argument('--latticeSize', '-l', default=None,
                        help='Lattice size, one number. i.e. 3 for a 3x3 lattice, leave empty for custom pattern')
    parser.add_argument('--outputLoc', '-o', default=None,
                        help='Location where the analysis will occur, Recommended: Using a local directory on the computer running this program will greatly speed up calculations, Default will be the same directory as the navigator.')
    parser.add_argument('--storageLoc', '-s', default=None,
                        help='Back up the files to a remote location after tilt series is acquired (will use rsync -au). If remote location, make sure you have ssh key and type in full path (i.e. user@hostname:/path/')
    parser.add_argument('--Refine', action='store_true', default=False,
                        help='Used to refine the image shift locations and generate coordinates, goes along with using the RefineIS.txt SerialEM script')
    parser.add_argument('--thresholds', nargs=2, type=float, default=[
                        0.1, 0.005], help='Two floats. Thresholds for the alignement of low-mag and high-mag tracking in microns, default 0.1 0.005. Good for <1.5 A/pix. You can disable low-mag tracking with -1')
    parser.add_argument('--centerShift', '-c', type=float, default=0.7,
                        help='Float corresponding to the center offset of the trial position along the tilt axis.')
    parser.add_argument('--defocus_range', '-def', nargs=3, type=float,
                        default=[-1.25, -2.5, 0.25], help='Three floats. corresponding to the lower and higher defocus target limits and the increment between each tilt series.')
    parser.add_argument('--custom_final', '-cf', action='store_true', default=False,
                        help='Only useful with the --Refine option is used with a holey pattern')
    parser.add_argument('--tilt_scheme', '-ts', nargs='+', type=int, default=None, help='Input the tilt scheme angle list')
    parser.add_argument('--tilt_scheme_file', '-tsf', type=str, default=None, help='Input the tilt scheme angle list from a file')
    parser.add_argument('--process', '-p', type=int, default=int(psutil.cpu_count(logical=False) // 2),
                        help='Number of processes to spawn for data processing. default: Half of physical cpus')
    parser.add_argument('--copyFrames', action='store_true', default=False,
                        help='To move Frames to outputLoc/Stack/ after tilt series is finished')

    args_cmd = vars(parser.parse_args())
    if args_cmd['navigatorPath'] is None:
        print('please provide navigator path')
        sys.exit(1)
    settingsFile = os.path.join(args_cmd['outputLoc'], 'settings.json')

    max_procs = psutil.cpu_count(logical=True)
    if args_cmd['process'] > max_procs:
        print(f'Your computer has {max_procs} threads. Number of --process should be lower or equal that value')
    else:
        print(f"Your computer has {max_procs} threads. Areas will be split into {args_cmd['process']} processes")

    if os.path.isfile(settingsFile):
        print('Found settings files')
        with open(settingsFile, 'r') as f:
            args = Settings(json.loads(f.read()))
        args.update(args_cmd)
        args.save(settingsFile)
    else:
        args = Settings(args_cmd)
        if not os.path.isdir(args.outputLoc):
            os.mkdir(args.outputLoc)
            os.mkdir(os.path.join(args.outputLoc, 'logs'))
    if args.copyFrames:
        framesdir = os.path.join(args.outputLoc, 'Stack')
        if not os.path.isdir(framesdir):
            os.mkdir(framesdir)
        # if args.storageLoc is not None and not os.path.isdir(args.storageLoc):
        #     os.mkdir(args.storageLoc)
        #     args.storageLoc = os.path.join(args.storageLoc)
        # else:
        #     args.storageLoc = os.path.join(args.outputLoc)
        args.save(settingsFile)

    if args.latticeSize is not None:
        if args.latticeSize == 'None':
            args.latteiceSize = None
        else:
            try:
                args.latticeSize = int(args.latticeSize)
            except ValueError:
                print('--latticeSize argument should be a number, exiting...')
                sys.exit(1)
    if args_cmd['tilt_scheme'] is None and args_cmd['tilt_scheme_file'] is None and args.tilt_scheme is None:
        print('No Tilt scheme specified, using default from config file')
        args.tilt_scheme = config._TILT_SCHEME
    elif args_cmd['tilt_scheme_file'] is not None:
        try:
            print(f'Reading tilt angles from {args.tilt_scheme_file}')
            with open(args.tilt_scheme_file, 'r') as f:
                tilt_scheme = [l for l in f.readlines() if l.strip() != '']
            args.tilt_scheme = [int(t.strip()) for t in tilt_scheme]
        except Exception as e:
            print('Could not read tilt scheme file')
            print(e)
            sys.exit(1)
    elif args_cmd['tilt_scheme'] is not None:
        args.tilt_scheme = args_cmd['tilt_scheme']

    for k, v in cfg._WINDOWS_MOUNT_LOC.items():
        if v in args.navigatorPath:
            drive = k
            print(f'Navigator Located in windows {drive} drive')
            break
        print('Could not locate the windowd drive associated with the navigator path, please edit config.py file')
        sys.exit(1)

    nav = Navigator(args.navigatorPath, latticeSize=args.latticeSize)
    nav.parse_nav_file()
    write_BISECT_script('/'.join(args.navigatorPath.split('/')[:-1]), drive)

    if args.Refine:
        if args.latticeSize is None:
            custom = True
        else:
            custom = args.custom_final
        nav.refine_navItems(drive, custom_final=custom)
        print('Created Coordinate files, run the refinement script in serialem')
        args.save(settingsFile)
        sys.exit(0)
    else:
        navItems = create_navItems(nav, args.outputLoc, args.tilt_scheme)
        args.save(settingsFile)

    for navI in navItems:
        navI.create_nav_item()
        positions = navI.createPositions(thresholds=args.thresholds, )  # storage=args.storageLoc
#         if not NavI._new:
#             NavI.recover_run()
        navI.create_serialem_script(drive, dfmin=max(args.defocus_range[0:1]), dfmax=min(args.defocus_range[0:1]),dfinc=args.defocus_range[2])
        procs = []
        center_parent_conn, center_child_conn = Pipe(False)
        p_center = Process(target=process_daemon, name=f'{navI.NavPosition}_Center', args=(positions['Center'], center_child_conn,))
        procs.append([p_center, center_parent_conn, center_child_conn])
        # for ind, pos in enumerate(positions['Areas']):
        areas_per_proc = -(-len(positions['Areas']) // args.process)
        for i in range(0, len(positions['Areas']), areas_per_proc):
            areas_parent_conn, areas_child_conn = Pipe(False)
            p_areas = Process(target=process_daemon, name=f"{navI.NavPosition}_Area_{i}-{min([len(positions['Areas'])-1,i+areas_per_proc-1])}",
                              args=(positions['Areas'][i:i + areas_per_proc], areas_child_conn,))
            procs.append([p_areas, areas_parent_conn, areas_child_conn])

        outputs = []
        for proc, parent, child in procs:
            proc.start()
            outputs.append(False)

        args.NavItems[navI.NavPosition]['Status'] = 'Started'
        args.save(settingsFile)

        counts = 0
        while not outputs == [True] * len(outputs):
            for ind, (proc, parent, child) in enumerate(procs):
                if parent.poll():
                    outputs[ind] = parent.recv()
                    if outputs[ind] is True:
                        proc.join()
            counts += 1
            if counts == 300:
                sub.Popen(["cleanup.py", settingsFile])
                counts = 0
            time.sleep(1)
        else:
            with open(settingsFile, 'r') as f:
                args = Settings(json.loads(f.read()))
            args.NavItems[navI.NavPosition]['Status'] = 'Finished'
            args.save(settingsFile)
            sub.Popen(["cleanup.py", settingsFile])
            print('Finished')
