import os
import pandas as pd


class Mdoc:

    def __init__(self, area, ordered = True):
        self.metadata = area.metadata
        self.stack = area.orderedStack if ordered else area.stack
        # print(self.__dict__)

    def extract_mdoc_fields(self):
        self.mdoc_meta = self.metadata.drop(columns=['xf','sumxf','df1','df2','angast','cc','ctffit','astig','df','ImageFile','eucentric'],errors='ignore')
        # print(self.mdoc_meta)


    def meta_to_mdoc(self, ordered=True):
        def pandas_to_mdoc_string(row):
            string = f'[ZValue = {row.name}]\n'
            for k,v in row.items():
                if isinstance(v,list):
                    v = ' '.join([str(i) for i in v])
                string += f'{k} = {v}\n'
            string += '\n'
            # print(string)
            return string
        if ordered:
            meta = self.mdoc_meta.sort_values(by=['TiltAngle'],ignore_index=True)
        meta_strings = meta.apply(pandas_to_mdoc_string,axis=1)
        return meta_strings.tolist()

    def header(self):
        meta = self.metadata.iloc[0]
        header = f'ImageFile = {self.stack}\n'
        for k,v in meta[['PixelSpacing','Voltage','ImageSize','DataMode']].items():
            header += f'{k} = {v}\n'

        header += f'\n[T = SerialEM                                    {meta.DateTime}    ]\n\n'
        header += f'[T =     Tilt axis angle = {meta.RotationAngle - 90}, binning = {meta.Binning}  spot = {meta.SpotSize}  camera = {meta.CameraIndex} bidir = 0.0]\n\n'
        return header

    def make_full_mdoc(self):
        self.full_mdoc = self.header() + ''.join(self.meta_to_mdoc())

    def save(self):
        with open(f'{self.stack}.mdoc','w') as f:
            f.write(self.full_mdoc)



