#!/usr/bin/env python

# BSD 3-Clause License

# Copyright (c) 2021, Bartesaghi lab (Duke University) and Borgnia lab (NIEHS/NIH)
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from BISECTomo.bisectomo import Settings
from BISECTomo.tomo_methods import Position, NavItem
import os
import argparse
import json
import subprocess as sub
import shlex

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Start the cleanup procedure for tomography')
    parser.add_argument('settings', default=None, help='Full path to the settings file')
    parser.add_argument('--reprocess', action='store_true', default=False)

    args_cmd = parser.parse_args()

    if os.path.isfile(args_cmd.settings):
        print('Found settings files')
        with open(args_cmd.settings, 'r') as f:
            args = Settings(json.loads(f.read()))

    for item, vals in args.NavItems.items():
        if vals['Status'] == 'Started' or vals['Status'] == 'Finished' or args_cmd.reprocess:
            m = vals['Status']
            print(f'Item {item} has {m} acquiring')
            navI = NavItem(vals['rootfolder'], vals['NavPosition'], tilt_scheme=args.tilt_scheme,
                           outfolder=args.outputLoc, ISareas=vals['ISareas'], IScontrol=[])
            pos = navI.createPositions()
            try:
                navI.generate_report_png(center_loc=args.centerShift)
            except:
                pass
            if vals['Status'] == 'Finished' or args_cmd.reprocess:
                
                for key, val in navI.Positions.items():
                    for pos in val:
                        #                            pos.generate_aligned_stack()
                        pos.generate_report_png()
                        pos.generate_ordered_stack()
                        pos.export_mdoc(ordered=True)
                        #                             pos.generate_gif(scale=0.3)
                        if args.copyFrames and key == 'Areas':
                            pos.move_frames(os.path.join(args.outputLoc, 'Stack'))

                #     pos.save_metadata()
                navI.cleanup()
                args.NavItems[item]['Status'] = 'Completed'
                args.save(args_cmd.settings)
                if args.storageLoc is not None:
                    print('Transfering files')
                    try:
                        if args.outputLoc[-1] == '/':
                            args.outputLoc = args.outputLoc[:-1]
                        if args.storageLoc[-1] == '/':
                            args.storageLoc = args.storageLoc[:-1]
                        com = f'rsync -au {args.outputLoc} {args.storageLoc}'
                        print(com)
                        sub.Popen(shlex.split(com))
                    except Exception as err:
                        print(err)
