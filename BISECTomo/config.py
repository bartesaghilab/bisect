#!/usr/bin/env python
import os
import sys

# Third-party applications paths
_IMOD_BIN = '/usr/local/IMOD/bin/'
_CTFFIND = '/usr/local/ctffind4/ctffind'

# Path to the microscope computer (Gatan computer)
_WINDOWS_MOUNT_LOC = {'X:': '/mnt/gatan_Raid_X'}
_TEMPLATE_FILES = os.path.join(os.path.dirname(os.path.abspath(__file__)), '../Template_files')

# Default tilt_scheme
_TILT_SCHEME = [0,
                3,
                -3, -6,
                6, 9,
                -9, -12,
                12, 15,
                -15, -18, -21,
                18, 21, 24, 27,
                -24, -27, -30, -33,
                30, 33, 36, 39, 42,
                -36, -39, -42, -45, -48,
                45, 48, 51, 54, 57, 60,
                -51, -54, -57, -60]

# Redifining _WINDOWS_MOUNT_LOC with the proper trailing / and \
_tmp_win_loc = _WINDOWS_MOUNT_LOC
_WINDOWS_MOUNT_LOC = dict()
for k, v in _tmp_win_loc.items():
    if k[-1] == '\\':
        k = k[:-1]
    if v[-1] != '/':
        v += '/'
    _WINDOWS_MOUNT_LOC[k] = v
