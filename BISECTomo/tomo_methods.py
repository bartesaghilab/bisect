#!/usr/bin/env python

# BSD 3-Clause License

# Copyright (c) 2021, Bartesaghi lab (Duke University) and Borgnia lab (NIEHS/NIH)
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# 1. Redistributions of source code must retain the above copyright notice, this
#    list of conditions and the following disclaimer.

# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.

# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from scipy import optimize, odr
import BISECTomo.config as cfg
from BISECTomo.genericposition import GenericPosition
import threading
import subprocess as sub
import matplotlib.gridspec as gridspec
import matplotlib.patches as patch
import matplotlib.pyplot as plt
import os
import re
import glob
import shutil
import time
from math import sqrt, atan2, cos, sin, radians, copysign
import numpy as np
import pandas as pd
import matplotlib as mpl
from BISECTomo.mdocfile import Mdoc
mpl.use('Agg')


def convert_path(path, _WINDOWS_MOUNT_LOC=cfg._WINDOWS_MOUNT_LOC):

    for k, v in _WINDOWS_MOUNT_LOC.items():
        if k in path:
            drive = v
            print(drive, k)
            path = path.replace('\\', '/')[len(k):]
            if path[0] == '/':
                path = path[1:]

    return os.path.join(drive, path)


def rename_frames(path, new_name):
    path = convert_path(path)
    new_path = '/'.join(path.split('/')[:-1] + [new_name])
    print(new_path)
    if os.path.isfile(path):
        shutil.move(path, new_path)
        shutil.move(f'{path}.mdoc', f'{new_path}.mdoc')
        return new_path
    elif os.path.isfile(new_path):
        print('File already renamed')
    else:
        print(f'File {path} does not exist')


def insert_append(inlist, value, index, fillvalue=None):
    diff = index - len(inlist)
    if diff > 0 and len(inlist) > 0:
        extend_list = [fillvalue] * (diff) + [value]
        outlist = inlist + extend_list
    elif diff > 0:
        outlist = [fillvalue] * (diff) + [value]
    elif len(inlist) == 0:
        outlist = [value]
    elif diff == 0:
        outlist = inlist + [value]
    else:
        inlist[index] = value
        outlist = inlist
    return outlist


class Navigator:
    def __init__(self, navigatorPath, latticeSize=2):
        self.rootFolder = '/'.join(navigatorPath.split('/')[:-1])
        self.navFile = navigatorPath
        self.latticeSize = latticeSize

    def parse_nav_file(self):
        with open(self.navFile, 'r')as file:
            nav = file.read().split('\n\n')
        self.navItems = dict()
        pattern = re.compile('StageXYZ\s=\s([-\d\.\s]+)\n')
        group = 'none'
        total_areas = 0
        item = False
        acquire = False
        for i in nav:
            try:
                Values = re.findall(pattern, i)[0]
            except IndexError:
                continue
            if re.search(re.compile('Acquire = 1'), i):
                if item:
                    print(f'\tFound {areas} BIS areas for {item}')
                item = re.findall(re.compile('\[Item = (.*)\]'), i)[0]
                group = re.findall(re.compile('GroupID = (\d*)'), i)[0]
                areas = 0
                self.navItems[item] = {'Center': [], 'Areas': []}
                self.navItems[item]['Center'] = np.array([float(i) for i in Values.split(' ')[:-1]])
                print(f'Adding area {item} for acquision')
                if not os.path.isdir(os.path.join(self.rootFolder, item)):
                    os.mkdir(os.path.join(self.rootFolder, item))
            elif re.search(re.compile(f'GroupID = {group}'), i) and type(self.latticeSize) != int:

                self.navItems[item]['Areas'].append(list((np.array([float(i) for i in Values.split(' ')[:-1]]) - self.navItems[item]['Center']) * -1))
                areas += 1
                total_areas += 1
                # print(f'\tAdding IS area to {item}')
        if item:
            print(f'\tFound {areas} BIS areas.')
        print(f'Total areas for acquision: {len(self.navItems)}.\nTotal tilt series: {total_areas}')

    def holeLattice(self):
        """
        Parse navigator file for the coordinate of the center of the lattice and one of the corner of a X hole lattice. This should be the first and second navigator entries respectively.
        Then will set up the lattice
        """
        if self.latticeSize > 1:
            with open(self.navFile, 'r') as file:
                nav = file.read()
            pattern = re.compile('StageXYZ\s=\s([-\d\.]+)\s([-\d\.]+)')
            center, hole = re.findall(pattern, nav)[0:2]

            xDist = float(hole[0]) - float(center[0])
            yDist = float(hole[1]) - float(center[1])
            vector = sqrt(xDist**2 + yDist**2)
            angle = radians(45)
            vector_angle = angle - atan2(yDist, xDist)

            x = np.linspace(-vector * sin(angle), vector * sin(angle), self.latticeSize)
            y = np.linspace(-vector * cos(angle), vector * cos(angle), self.latticeSize)
            for j in y:
                try:
                    yn = np.append(yn, np.full((1, len(y)), j))
                    xn = np.append(xn, x)
                except:
                    xn = x
                    yn = np.full((1, len(y)), j)
            xn_rotated = xn * cos(vector_angle) + yn * sin(vector_angle)
            yn_rotated = yn * cos(vector_angle) - xn * sin(vector_angle)

            self.ISareas = [[i, j] for i, j in zip(xn_rotated.tolist(), yn_rotated.tolist())]

        print('Lattice of {0} hole(s) X {0} hole(s) initialized'.format(int(len(self.ISareas)**0.5)))

    def refine_navItems(self, drive, custom_final=False):
        if type(self.latticeSize) == int:
            holey = True
            self.holeLattice()
            with open(os.path.join(self.rootFolder, 'global_coord.txt.lock'), 'w') as f:
                for x, y in self.ISareas:
                    f.write(f'{x} {y}\n')
            shutil.move(os.path.join(self.rootFolder, 'global_coord.txt.lock'), os.path.join(self.rootFolder, 'global_coord.txt'))
        else:
            holey = False
            for item in self.navItems:
                if not os.path.isdir(os.path.join(self.rootFolder, item)):
                    os.mkdir(os.path.join(self.rootFolder, item))
                with open(os.path.join(self.rootFolder, item, 'coord.txt.lock'), 'w') as f:
                    for x, y in self.navItems[item]['Areas']:
                        f.write(f'{x} {y}\n')
                shutil.move(os.path.join(self.rootFolder, item, 'coord.txt.lock'), os.path.join(self.rootFolder, item, 'coord.txt'))

        with open(os.path.join(cfg._TEMPLATE_FILES, 'Refine_IS.txt'), 'r') as f:
            script = f.read()
        file = os.path.join(self.rootFolder, 'Refine_Script.txt')
        with open(file + '.lock', 'w') as f:
            f.write(script.format(location=drive + self.rootFolder[len(cfg._WINDOWS_MOUNT_LOC[drive]) - 1:].replace('/', '\\'),
                                  holey=int(holey),
                                  custom_final=int(custom_final)))
        shutil.move(file + '.lock', file)

    def parse_refined_navItems(self):
        print('Loading refined coordinates')
        if type(self.latticeSize) == int:
            with open(os.path.join(self.rootFolder, 'refined_global_coord.txt'), 'r') as f:
                self.ISareas = [[float(i), float(j)] for i, j in (l.split(' ') for l in f.readlines())]
            print(f'\tFound {len(self.ISareas)} areas.')
        else:
            for item in self.navItems:
                with open(os.path.join(self.rootFolder, item, 'refined_coord.txt'), 'r') as f:
                    self.navItems[item]['Areas'] = [[float(i), float(j)] for i, j in (l.split(' ') for l in f.readlines())]
                print(f"\tFound {len(self.navItems[item]['Areas'])} IS areas for item {item}.")


class NavItem:

    _new = True

    def __init__(self, rootfolder, NavPosition, tilt_scheme=None, outfolder=None, ISareas=None, data_type='Tomo', **kwargs):
        print(f'Initiating navigator position {NavPosition}')
        self.NavPosition = NavPosition
        self.rootFolder = rootfolder
        self.ISareas = ISareas
        self.Positions = {'Center': [], 'Areas': []}
        self.root = os.path.join(self.rootFolder, self.NavPosition)
        if outfolder is not None:
            self.outfolder = os.path.join(outfolder, self.NavPosition)
        else:
            self.outfolder = self.root
        if tilt_scheme is not None:
            self.tilt_scheme = tilt_scheme

        self.settings = os.path.join(self.root, 'Tomo_script.txt')

    def cleanup(self):
        temp_files = glob.glob(os.path.join(self.outfolder, '*', '*~'))
        for f in temp_files:
            os.remove(f)

    def create_nav_item(self):
        if os.path.isfile(self.settings):
            self._new = False
            print('Navigatior Item directory already exists')

        for folder in [self.root, self.outfolder]:
            if folder is not None:
                if not os.path.isdir(folder):
                    print(f'Creating {folder}')
                    os.mkdir(folder)

    def convert_to_serialem_relative_path(self, path):
        return '{}'.format(path[len(self.root) + 1:].replace('/', '\\'))

    def create_serialem_script(self, drive, dfmin=-1.25, dfmax=-2.25, dfinc=0.25):
        with open(os.path.join(cfg._TEMPLATE_FILES, 'Tomo_script.txt'), 'r') as f:
            script = f.read()
        center_scripts = ''
        for center in self.Positions['Center']:
            if 'hm' in center.name:
                mag = 'T'
            else:
                mag = 'V'
            with open(os.path.join(cfg._TEMPLATE_FILES, 'centering_script.txt'), 'r') as f:
                script_center = f.read()
            center_scripts += script_center.format(mag=mag,
                                                   image=self.convert_to_serialem_relative_path(center.image),
                                                   analyzed=self.convert_to_serialem_relative_path(center.analyzedFile),)
        with open(self.settings + '.lock', 'w') as f:
            f.write(script.format(location=drive + self.rootFolder[len(cfg._WINDOWS_MOUNT_LOC[drive]) - 1:].replace('/', '\\'),
                                  tiltScheme='{ ' + ' '.join([str(i) for i in self.tilt_scheme]) + ' }',
                                  center_scripts=center_scripts,
                                  xs='{ ' + ' '.join([str(p.x) for p in self.Positions['Areas']]) + ' }',
                                  ys='{ ' + ' '.join([str(p.y) for p in self.Positions['Areas']]) + ' }',
                                  imgs='{ ' + ' '.join([self.convert_to_serialem_relative_path(p.image) for p in self.Positions['Areas']]) + ' }',
                                  dfmin=dfmin,
                                  dfmax=dfmax,
                                  dfinc=dfinc))
        shutil.move(self.settings + '.lock', self.settings)

    def createPosition(self, name, posType, **kwargs):
        destFolder = os.path.join(self.root, posType)
        outfolder = os.path.join(self.outfolder, posType)
        if posType == 'center':
            if 'hm' in name:
                self.Positions['Center'].append(Center(destFolder, name, outFolder=outfolder, **kwargs))
            else:
                self.Positions['Center'].append(Center(destFolder, name, outFolder=outfolder, **kwargs))
        elif posType == 'area':
            self.Positions['Areas'].append(CollectionArea(destFolder, name, outFolder=outfolder, **kwargs))

    def createPositions(self, outfolder=None, thresholds=[0.02, 0.002], **kwargs):
        if thresholds[0] != -1:
            print('Creating Center position')
            self.createPosition('center', 'center', tiltScheme=self.tilt_scheme, threshold=thresholds[0], defocus=False, **kwargs)
        print('Creating High-mag center position')
        self.createPosition('center_hm', 'center', tiltScheme=self.tilt_scheme, threshold=thresholds[1], **kwargs)

        for ind, (x, y) in enumerate(self.ISareas):
            print(f'Creating acquisision area {ind}, with IS  x= {round(x,2)} um and y= {round(y,2)} um')
            self.createPosition(str(ind), 'area', x=x, y=y, tiltScheme=self.tilt_scheme, **kwargs)
        return self.Positions

    def generate_report_png(self, center_loc=1.7):
        cmap = mpl.cm.seismic
        fig = plt.figure(figsize=(12, 10))
        spec = gridspec.GridSpec(ncols=12, nrows=1, figure=fig, wspace=0.1, hspace=2.5)
        ax1 = fig.add_subplot(spec[:, :-1])

        for _, val in self.Positions.items():
            for c in val:
                if c.name != 'center':
                    if 'center' in c.name:
                        c.x, c.y = center_loc, 0
                    x, y = [], []
                    x_fov = c.x - c.metadata.ImageSize.iloc[-1][0] / 2 * c.metadata.PixelSpacing.iloc[-1] / 10000
                    y_fov = c.y - c.metadata.ImageSize.iloc[-1][1] / 2 * c.metadata.PixelSpacing.iloc[-1] / 10000
                    w = c.metadata.ImageSize.iloc[-1][0] * c.metadata.PixelSpacing.iloc[-1] / 10000
                    l = c.metadata.ImageSize.iloc[-1][1] * c.metadata.PixelSpacing.iloc[-1] / 10000
                    rect = patch.Rectangle((x_fov, y_fov), w, l, linewidth=1, edgecolor='r', facecolor='none')
                    t_start = ax1.transData
                    t = mpl.transforms.Affine2D().rotate_deg_around(c.x,c.y,c.metadata.RotationAngle.iloc[-1])
                    rect.set_transform(t + t_start)
                    ax1.add_patch(rect)
                    ax1.axes.text(x_fov - w * 0.1, y_fov + l * 1.1, str(c.name), c='r', fontsize=12)
                    for i, j in c.metadata.sumxf:
                        x.append(i + c.x)
                        y.append(j + c.y)
                    ax1.scatter(x, y, c=c.metadata.TiltAngle.round().astype('Int64'), cmap=cmap, linewidths=1, edgecolors='black')

        plt.xlabel('Relative Stage X (microns)')
        plt.ylabel('Relative Stage Y (microns)')
        xmin, xmax, ymin, ymax = plt.axis('scaled')
        ax1.axhline(y=0, xmin=xmin, xmax=xmax, c='b')
        ax1.axes.text(xmin + xmax * 0.03, ymax * 0.02, 'Tilt Axis', c='b', fontsize=12)
        ax3 = fig.add_subplot(spec[:, -1])
        norm = mpl.colors.Normalize(vmin=min(c.metadata.TiltAngle.round().astype('Int64')), vmax=max(c.metadata.TiltAngle.round().astype('Int64')))
        cb1 = mpl.colorbar.ColorbarBase(ax3, cmap=cmap, norm=norm, orientation='vertical')
        plt.ylabel('Tilt Angle (degrees)')
        plt.savefig(os.path.join(self.outfolder, f'{self.NavPosition}_summary.png'))
        plt.close(os.path.join(self.outfolder, f'{self.NavPosition}_summary.png'))
        plt.close(fig='all')

    # def recover_run(self):
    #     if 'metadata' in self.__dict__:
    #         tilts_done = min([len(pos.metadata.index) for pos in self.Positions])
    #         self.tilt_scheme = self.tilt_scheme[tilts_done:]
    #         skipto = 'Start'
    #         for pos in self.Positions:
    #             if pos.name == 'center_hm':
    #                 shiftminus = ' '.join([str(i) for i in pos.metadata.ImageShift[pos.metadata[['TiltAngle']].idxmin()].tolist()[0]])
    #                 shiftplus = ' '.join([str(i) for i in pos.metadata.ImageShift[pos.metadata[['TiltAngle']].idxmax()].tolist()[0]])
    #             if len(pos.metadata) > tilts_done:
    #                 if pos.name == 'center':
    #                     skipto = 'Center_hm'
    #                 elif pos.name == 'center_hm':
    #                     skipto = 'Areas'
    #         print(
    #             f'Recovering run:\n\tWill start at tilt angle {self.tilt_scheme[0]}\n\tISminus: {shiftminus}\n\tISplus: {shiftplus}\n\tWill skip to: {skipto}')

    #         with open(os.path.join(self.root, 'run_recovery.txt'), 'w') as file:
    #             file.write(f'{shiftminus}\n{shiftplus}\n{skipto}')

    # def navitem_control(self):
    #     self.create_nav_item()
    #     self.createPositions()
    #     if not self._new:
    #         self.recover_run()
    #     self.create_serialem_settings()
    #     print('Starting tilt series monitoring')
    #     tilting = True
    #     threads = list()
    #     while tilting:
    #         for _, val in self.Positions.items():
    #             for v in val:
    #                 t = threading.Thread(target=v.logic, args=())
    #                 threads.append(t)
    #                 t.start()
    #         for index, thread in enumerate(threads):
    #             thread.join()
    #         if self.Positions[-1].finished:
    #             tilting = False
    #             self.generate_report_png()
    #         time.sleep(5)
    #     print('Collection Done. Going to the next navigator Item')


class Position(GenericPosition):

    _command = os.path.join(cfg._IMOD_BIN, 'newstack')

    def __init__(self, destFolder, name, outFolder=None, threshold=0.003, defocus=True, create_dirs=True, **kwargs):
        self.destFolder = destFolder
        self.navI = self.destFolder.split('/')[-2]
        self.name = name

        self.threshold = threshold
        self.defocus = defocus
        if outFolder is None:
            outFolder = destFolder
        self.outFolder = outFolder

        kwargs.setdefault('image', os.path.join(destFolder, f'{name}.mrc'))
        kwargs.setdefault('mdoc', os.path.join(destFolder, f'{name}.mrc.mdoc'))
        kwargs.setdefault('stack', os.path.join(outFolder, f'{name}_stack.mrc'))
        kwargs.setdefault('analyzedFile', os.path.join(destFolder, f'{name}_analyzed.txt'))
        kwargs.setdefault('tempStack', os.path.join(outFolder, f'{name}_tempstack.mrc'))
        kwargs.setdefault('xfFile', os.path.join(outFolder, f'{name}_ali.txt'))
        kwargs.setdefault('alixfFile', os.path.join(outFolder, f'{name}_ali.xf'))
        kwargs.setdefault('alignedStack', os.path.join(outFolder, f'{name}_ali.mrc'))
        kwargs.setdefault('orderedStack', os.path.join(outFolder, f'{name}_ordered.mrc'))
        kwargs.setdefault('metadataFile', os.path.join(outFolder, f'{name}_metadata.pkl'))
        kwargs.setdefault('metadataIterFile', os.path.join(outFolder, f'{name}_metadataIter.pkl'))
        kwargs.setdefault('ctf', os.path.join(outFolder, f'{name}_ctf'))
        kwargs.setdefault('storage', os.path.join(outFolder))
        kwargs.setdefault('IScoords', f'{self.destFolder}/IS_coords.txt')

        for k, v in kwargs.items():
            setattr(self, k, v)

        if create_dirs:
            self.create_directories()
        #     print(f'Creating directory: {self.storage}')
        #     os.mkdir(self.storage)

        if os.path.isfile(self.metadataFile):
            print('Found metadata file, reading...')
            self.read_metadata()

        self.finished = False

    def create_directories(self):
        if not os.path.isdir(self.destFolder):
            os.mkdir(self.destFolder)
        if not os.path.isdir(self.outFolder):
            os.mkdir(self.outFolder)
        if not os.path.isdir(self.storage):
            print(f'Creating directory: {self.storage}')
            os.mkdir(self.storage)

    
    def check_if_image(self):
        return os.path.isfile(self.image) and os.path.isfile(self.mdoc)
    
    def remove_image(self):
        os.remove(self.image)
        os.remove(self.mdoc)

    def parse_xf(self):
        """
        Opens the transform file from IMOD and adds the values for each side of the series to calculate the sum of the alignment. Finally returns the plus of minus side of the tilt dependeing on the tilt angle being recorded
        """
        with open(self.xfFile) as file:
            xf = file.readlines()
        tilts = [int(round(i)) for i in self.metadata.TiltAngle]
        pattern = re.compile('\s([\d\.-]+)[\s]+([\d\.-]+)\n')
        plus = [0, 0]
        minus = [0, 0]
        xfdf = pd.DataFrame()
        for tilt, x in zip(tilts, xf):
            x = re.findall(pattern, x)[0]
            x = self.pixel_to_specimen_coords(float(x[0]) * self.metadata.PixelSpacing.iloc[-1] / 10000,
                                              float(x[1]) * self.metadata.PixelSpacing.iloc[-1] / 10000,)
            if tilt < 0:
                minus[0] += x[0]
                minus[1] += x[1]
                xfdf = xfdf.append(pd.DataFrame({'xf': [list(x)], 'sumxf': [list(minus)]}), ignore_index=True)
            else:
                plus[0] += x[0]
                plus[1] += x[1]
                xfdf = xfdf.append(pd.DataFrame({'xf': [list(x)], 'sumxf': [list(plus)]}), ignore_index=True)

        self.metadata = pd.merge(xfdf, self.metadata, how='outer', left_index=True, right_index=True, on=list(self.metadata.keys() & xfdf.keys()))
        # print(self.metadata)
        return self

    def drop_meta_rows(self, n=1):
        if 'metadataIter' not in self.__dict__:
            print('Initiating metadaIter')
            self.metadataIter = self.metadata.tail(n)
        else:
            print('Appending metadataIter')
            self.metadataIter = pd.concat([self.metadataIter, self.metadata.tail(n)], ignore_index=False, sort=False)
        self.metadata.drop(self.metadata.tail(n).index, inplace=True)

    def calculate_correction(self):
        print('Aligning center')
        self.align_tilt(iterate=2)
        self.parse_xf()
        self.corr_x, self.corr_y = self.metadata.sumxf.iloc[-1]
        self.dist = sqrt(self.corr_x ** 2 + self.corr_y ** 2)
        print(f'Measured distance: {self.dist} um \nx:{self.corr_x} um \ny:{self.corr_y} Threshold: {self.threshold} um')

    @property
    def imodExtraRotation(self):
        return self.metadata.RotationAngle.iloc[-1] + 90
        # if -45 <= self.metadata.RotationAngle.iloc[-1] <= 45:
        #     return 90
        # elif  45 < self.metadata.RotationAngle.iloc[-1] < 135:
        #     return 0
        # elif -45 > self.metadata.RotationAngle.iloc[-1] > -135:
        #     return 180
        # elif -135 >= self.metadata.RotationAngle.iloc[-1] >= -180:
        #     return 270
        # elif 135 <= self.metadata.RotationAngle.iloc[-1] <= 180:
        #     return -90
        


    def align_tilt(self, tempstack=True, add_to_stack=True, iterate=3):
        """
        Open an mrc stack and calculate image alignment. Will be used for calculating the alignments of the control areas of the tilt series.
        """

        if tempstack:
            stack = self.tempStack
        else:
            stack = self.stack

        if add_to_stack:
            sub.run(f'{self._command} {self.stack} {self.image} {stack}'.split(' '), stdout=sub.DEVNULL)
        angles = ','.join([str(round(i)) for i in self.metadata.TiltAngle])
        sub.run(f'-inp {stack} -ou {self.xfFile} -angles {angles} -ro {self.imodExtraRotation} -radius1 0.050000 -sigma1 0.030000 -radius2 0.100000 -sigma2 0.030000 -iterate {iterate}'.split(' '),
                executable=os.path.join(cfg._IMOD_BIN, 'tiltxcorr'), stdout=sub.DEVNULL)
        return self

    def generate_aligned_stack(self):
        print(f'Generating aligned stack {self.alignedStack}')
        p = sub.run(f'-in {self.xfFile} {self.alixfFile}'.split(' '), executable=os.path.join(cfg._IMOD_BIN, 'xftoxg'), stdout=sub.PIPE)
        p = sub.run(f'{self._command} -xform {self.alixfFile} -in {self.stack} -ou {self.alignedStack}'.split(' '), stdout=sub.PIPE)
        return self

    def generate_ordered_stack(self, ali=False, tilts=None):
        if ali:
            stack = self.alignedStack
        else:
            stack = self.stack
        if tilts is None:
            tilts = [int(round(i)) for i in self.metadata.TiltAngle]
        inds = [x for x in range(0, len(tilts), 1)]
        sorted_tilts = [i for _, i in sorted(zip(tilts, inds))]
        order = ','.join(str(i) for i in sorted_tilts)
        print(f'Generating ordered stack {self.orderedStack}')
        p = sub.run(f'{self._command} -in {stack} -ou {self.orderedStack} -secs {order}'.split(' '), stdout=sub.PIPE)
        return self

    def generate_gif(self, scale=0.5, output=None):
        if output is None:
            output = f'{self.orderedStack}.gif'
        p = sub.run(f'{cfg._EMAN_BIN}/python {cfg._EMAN_BIN}/e2proc2d.py {self.orderedStack} {self.orderedStack}.hdf'.split(' '), stdout=sub.PIPE)
        p = sub.run(f'{cfg._EMAN_BIN}/python {cfg._EMAN_BIN}/e2stackanim.py {self.orderedStack}.hdf {output} --scale {scale} --pingpong'.split(' '), stdout=sub.PIPE)
        os.remove(f'{self.orderedStack}.hdf')

    def generate_report_png(self, defocus=True, save=True):
        File = self.stack.split('.')[0]
        cmap = mpl.cm.seismic
        fig = plt.figure(figsize=(10, 10),) #constrained_layout=True)
        fig.suptitle(self.name, fontsize=12)
        spec = gridspec.GridSpec(ncols=9, nrows=8, figure=fig, wspace=0.1, hspace=2.5)

        if self.defocus:
            ax1 = fig.add_subplot(spec[0:2, 0:5])
            ax1.scatter(self.metadata.TiltAngle.round().astype('Int64'), -self.metadata.df / 10000,
                        c=self.metadata.ctffit, cmap=mpl.cm.inferno, linewidths=1, edgecolors='black')
            ax1.set_title('Defocus')
            plt.xlabel('Tilt Angle (degrees)')
            plt.ylabel('Defocus(um)')
            ax4 = fig.add_subplot(spec[0:2, 5])
            norm = mpl.colors.Normalize(vmin=min(self.metadata.ctffit), vmax=max(self.metadata.ctffit))
            cb1 = mpl.colorbar.ColorbarBase(ax4, cmap=mpl.cm.inferno, norm=norm, orientation='vertical')
            plt.ylabel('CTF fit resolution (A)')


#         ax5 = fig.add_subplot(spec[0:2,6:])
#         ax5.scatter(self.metadata.real_y,self.metadata.real_z,cmap=cmap,c=self.metadata.TiltAngle.round().astype('Int64'),linewidths=1, edgecolors='black',label='Data')
#         circ = patch.Circle([self.metadata.euc_x.iloc[-1],self.metadata.eucentric.iloc[-1]],self.metadata.y.iloc[-1],edgecolor='blue',fill=False,label='Fit')
#         ax5.scatter(self.metadata.euc_x.iloc[-1],self.metadata.eucentric.iloc[-1],c='blue',marker='s')
#         ax5.add_patch(circ)
#         ax5.yaxis.set_label_position("right")
#         ax5.yaxis.tick_right()
#         ax5.set_title(f'Eucentric Fit: Y= {round(self.metadata.euc_x.iloc[-1],2)} Z= {round(self.metadata.eucentric.iloc[-1],2)}')
#         ax5.set_xlabel(u'Y position (\u03bcm)')
#         ax5.set_ylabel(u'Z position (\u03bcm)')
#         ax5.set_aspect(aspect=1)
        if self.__class__.__name__ == 'CollectionArea':
            ax5 = fig.add_subplot(spec[0:2,7:])
            ax5.scatter(self.metadata.TiltAngle,self.metadata.eucentric,linewidths=1, edgecolors='black',label='Data')
            ax5.yaxis.set_label_position("right")
            ax5.yaxis.tick_right()
            ax5.set_title(f'Predicted Eucentric Height')
            ax5.set_xlabel(u'Tilt Angle (degrees)')
            ax5.set_ylabel(u'Delta encentric from center (\u03bcm)')
            # ax5.axhline(y=-(self.metadata.TargetDefocus.iloc[0] + self.metadata.df.iloc[0] / 10000))
            axis_range = abs(self.metadata.eucentric.max() - self.metadata.eucentric.min())
            ax5.set_ylim([self.metadata.eucentric.min() - 0.1 * axis_range,self.metadata.eucentric.max() + 0.1 * axis_range])
        
        if self.__class__.__name__ == 'Center':
            ax5 = fig.add_subplot(spec[0:2,7:])
            defocus = self.metadata.Defocus + self.metadata.TargetDefocus
            ax5.scatter(self.metadata.TiltAngle,defocus.cumsum(),linewidths=1, edgecolors='black',label='Data')
            ax5.yaxis.set_label_position("right")
            ax5.yaxis.tick_right()
            ax5.set_title(f'Z-variation')
            ax5.set_xlabel(u'Tilt Angle (degrees_')
            ax5.set_ylabel(u'Defocus (\u03bcm)')

        # ax5.set_aspect(aspect=1)


        ax2 = fig.add_subplot(spec[2:, 0:-1])
        x, y = [], []
        for i, j in self.metadata.sumxf:
            x.append(i * 1000)
            y.append(j * 1000)
        ax2.scatter(x, y, c=self.metadata.TiltAngle.round().astype('Int64'), cmap=cmap, linewidths=1, edgecolors='black')
        ax2.set_title('Tracking')
        ax2.set_xlabel('Movement in X (nm)')
        ax2.set_ylabel('Movement in Y (nm)')
        

        ax2.set_xlim([-self.metadata.ImageSize.iloc[-1][0] / 2 * self.metadata.PixelSpacing.iloc[-1] / 10,
                      self.metadata.ImageSize.iloc[-1][0] / 2 * self.metadata.PixelSpacing.iloc[-1] / 10])
        ax2.set_ylim([-self.metadata.ImageSize.iloc[-1][1] / 2 * self.metadata.PixelSpacing.iloc[-1] / 10,
                      self.metadata.ImageSize.iloc[-1][1] / 2 * self.metadata.PixelSpacing.iloc[-1] / 10])
        ax2.set_aspect(aspect='equal')
        ax3 = fig.add_subplot(spec[2:, -1])
        norm = mpl.colors.Normalize(vmin=min(self.metadata.TiltAngle.round().astype('Int64')),
                                    vmax=max(self.metadata.TiltAngle.round().astype('Int64')))
        cb1 = mpl.colorbar.ColorbarBase(ax3, cmap=cmap, norm=norm, orientation='vertical')
        ax3.set_ylabel('Tilt Angle (degrees)')

        if save is True:
            plt.savefig(f'{File}.png')
            plt.close(f'{File}.png')
            plt.close(fig='all')

    def CTFfind(self, stack=False):
        if stack:
            img = self.stack
            extraparam = 'no\n'
        else:
            img = self.image
            extraparam = ''

        p = sub.run([cfg._CTFFIND], stdout=sub.PIPE,
                    input=f'{img}\n{extraparam}{self.ctf}.mrc\n{self.metadata.PixelSpacing.iloc[-1]}\n{self.metadata.Voltage.iloc[-1]}\n2.7\n0.07\n512\n30\n15\n5000\n50000\n200\nno\nno\nno\nno\nno',
                    encoding='ascii')
        with open(f'{self.ctf}.txt', 'r') as f:
            lines = [[float(j) for j in i.split(' ')] for i in f.readlines() if '#' not in i]

        ctf = pd.DataFrame.from_records(lines, columns=['l', 'df1', 'df2', 'angast', 'phshift', 'cc', 'ctffit'], exclude=[
            'l', 'phshift'], index=[self.metadata.index.values[-1]] if not stack else None)
        ctf['astig'] = ctf.df1 - ctf.df2
        ctf['df'] = (ctf.df1 + ctf.df2) / 2
        self.metadata = pd.merge(ctf, self.metadata, how='outer', left_index=True, right_index=True, on=list(self.metadata.keys() & ctf.keys()))
        for f in glob.glob(f'{self.ctf}*'):
            os.remove(f)
        return self

    def refine_eucentric_odr(self, write_file=True, initial=False, max_angle=45, num_pt=None):

        def calc_R(x, y, xc, yc):
            """ calculate the distance of each 2D points from the center (xc, yc) """
            return np.sqrt((x)**2 + (y - yc)**2)

        def f_3(beta, x):
            """ implicit definition of the circle """
#             return (x[0]-beta[0])**2 + (x[1]-beta[1])**2 -(beta[2]+copysign(beta[0],beta[2]))**2
            return (x[0] - beta[0])**2 + (x[1] - beta[1])**2 - beta[2]**2

        def calc_y_z(R, ang, z0, z, y0):
            #             y = y0 + ((R+np.copysign(y0,R)) * np.cos(np.radians(-ang)) + (z0-z) * np.sin(np.radians(-ang)))
            #             z = z + (-(R+np.copysign(y0,R)) * np.sin(np.radians(-ang)) + (z0-z) * np.cos(np.radians(-ang)))
            y = y0 + ((R) * np.cos(np.radians(-ang)) + (z0 - z) * np.sin(np.radians(-ang)))
            z = z + (-(R) * np.sin(np.radians(-ang)) + (z0 - z) * np.cos(np.radians(-ang)))
            return [y, z]

        if not initial:
            beta0 = [0, -self.metadata.df.iloc[0] / 10000, self.y]
            if len(self.metadata.index) > 1:
                y, z = calc_y_z(self.y, np.array(self.metadata.TiltAngle), -self.metadata.df.iloc[0] / 10000, (self.metadata.eucentric.shift(
                    periods=1, fill_value=0)), (self.metadata.euc_x.shift(periods=1, fill_value=0)))
                self.metadata['real_y'] = y - self.metadata.sumxf.apply(pd.Series)[1]
                self.metadata['real_z'] = z - self.metadata.sumxf.apply(pd.Series)[1] * np.tan(np.radians(-self.metadata.TiltAngle))
            else:
                self.metadata['real_y'] = self.y
                self.metadata['real_z'] = -self.metadata.df.iloc[0] / 10000
        else:
            beta0 = [0, 0, self.y]

        if len(self.metadata.index) >= 7:
            meta = self.metadata.loc[(self.metadata.ctffit < 30) & (abs(self.metadata.TiltAngle) < max_angle)]
            if num_pt is not None:
                meta = meta.iloc[:num_pt]
            lsc_data = odr.Data(np.array(meta[['real_y', 'real_z']]).transpose(), y=1)
            lsc_model = odr.Model(f_3, implicit=True)
            if len(meta.index) >= 15:
                ifix = [1, 1, 1]
            else:
                ifix = [0, 1, 0]
            lsc_odr = odr.ODR(lsc_data, lsc_model, beta0, ifixb=ifix)
            lsc_out = lsc_odr.run()

            xc, yc, R = lsc_out.beta
            print(xc, yc, R)
            Ri = calc_R(meta.real_y, meta.real_z, xc, yc)
            residu_3 = sum((Ri - R)**2)

        else:
            xc, yc, R = beta0

        if not initial:
            self.metadata.at[len(self.metadata.index) - 1, 'eucentric'] = yc
            self.metadata.at[len(self.metadata.index) - 1, 'euc_x'] = xc
            self.metadata.at[len(self.metadata.index) - 1, 'y'] = R
            df0 = -self.metadata.df.iloc[0] / 10000
        else:
            df0 = 0

        print(f'df0={df0}')
        ys, zs = calc_y_z(R, np.array(self.tiltScheme), df0, yc, xc)
        zs -= zs[0]

        if write_file:
            fname = f'{self.destFolder}/IS_{self.name}.txt'
            with open(fname + '.lock', 'w') as f:
                f.write(' '.join(ys.astype(str)) + '\n')
                f.write(' '.join(zs.astype(str)))
            shutil.move(fname + '.lock', fname)
        else:
            return ys, zs

    def refine_eucentric(self, write_file=True, initial=False):
        def eucentric(x, a):
            return ((x[1]) * np.cos(np.radians(-x[0])) + (a) * np.sin(np.radians(-x[0])))

        def eucentric_z(x, a):
            return (-(x[1]) * np.sin(np.radians(-x[0])) + (a) * np.cos(np.radians(-x[0])))

        if not initial:
            if len(self.metadata.index) >= 5:
                # print(self.metadata.eucentric.iloc[-1], type(self.metadata.eucentric.iloc[-1]))
                params, params_covariance = optimize.curve_fit(eucentric, np.array([-self.metadata.TiltAngle, [self.y] * len(self.metadata.index)]), np.array(self.metadata.sumxf.apply(
                    pd.Series)[1] + (self.y * np.cos(np.radians(-self.metadata.TiltAngle)) + (self.metadata.eucentric.shift(periods=1, fill_value=0)) * np.sin(np.radians(-self.metadata.TiltAngle)))), p0=[2])
                # print(f'Area {self.name} eucentric fit:\n',params, params_covariance )
                self.metadata.at[len(self.metadata.index) - 1, 'eucentric'] = params[0]

            else:
                params = [0] #[-(self.metadata.TargetDefocus.iloc[0] + self.metadata.df.iloc[0] / 10000)]
                self.metadata.at[len(self.metadata.index) - 1, 'eucentric'] = params[0]
                
            index = len(self.metadata.index)
            print(f'Area {self.name} eucentric:\n', self.metadata.eucentric)
        else:
            params = [0]
            index = 0
        if index == len(self.tiltScheme):
            return
        ys = eucentric(np.array([[self.tiltScheme[index]], [self.y]]), params[0])
        zs = eucentric_z(np.array([self.tiltScheme, [self.y] * len(self.tiltScheme)]), params[0])
        zs -= zs[0]
        zs = zs[index]
        
        if write_file:

            fname = f'{self.destFolder}/IS_{self.name}.txt'
            with open(fname + '.lock', 'w') as f:
                f.write(' '.join(ys.astype(str)) + '\n')
                f.write(str(zs))
            shutil.move(fname + '.lock', fname)
        else:
            return ys, zs

    def move_frames(self, new_path):
        def copy_frames(x, new_path):
            try:
                shutil.copy2(x, new_path)
                shutil.copy2(f'{x}.mdoc', new_path)
            except Exception as err:
                print('Error copying frames:', err)

        self.metadata['SubFramePath'].apply(copy_frames, new_path=new_path)

    def export_mdoc(self, ordered=True):
        m = Mdoc(self, ordered=ordered)
        m.extract_mdoc_fields()
        m.make_full_mdoc()
        m.save()


    # def modify_metadata_frames_path(self, new_path, is_windows=True, convert=True, rename=True):
    #     def modify_path(x, sep, new_path):
    #         x = x.split(sep)[-1]
    #         return os.path.join(new_path, x)

    #     def convert_to_linux_path(row, path_linux=None, new_path=None):
    #         path_win = row['SubFramePath'].split('\\')
    #         fname = os.path.join(cfg._MOUNT_LOC, '/'.join(path_win[1:]))
    #         dirpath = '/'.join(fname.split('/')[:-1])
    #         fname_new = fname.replace(fname[len(dirpath) + 1:], f'{self.navI}_{self.name}_{str(row.name).zfill(2)}_{int(round(row.TiltAngle))}.tif')
    #         if new_path is not None:
    #             return [fname, os.path.join(new_path, fname_new.split('/')[-1])]
    #         else:
    #             return [fname, os.path.join(dirpath, fname_new.split('/')[-1])]

    #     def rename(x):
    #         if os.path.isfile(x[0]):
    #             print(f'Copying {x[0]} to {x[1]}')
    #             shutil.copy(x[0], x[1])
    #             shutil.copy(f'{x[0]}.mdoc', f'{x[1]}.mdoc')
    #             if os.path.getsize(x[0]) == os.path.getsize(x[1]):
    #                 os.remove(x[0])
    #                 os.remove(f'{x[0]}.mdoc')
    #         elif os.path.isfile(x[1]):
    #             print('File already copied')
    #         else:
    #             print(f'File {x[0]} does not exist')

    #     new_path = os.path.join(new_path, 'Stack')
    #     if convert is False:
    #         if is_windows:
    #             sep = '\\'
    #         else:
    #             sep = '/'

    #         self.metadata['Frames'] = self.metadata.apply(modify_path, axis=1, sep=sep, new_path=new_path)

    #     else:
    #         self.metadata['Frames'] = self.metadata.apply(convert_to_linux_path, axis=1, new_path=new_path)

    #     if rename:
    #         if new_path is not None and not os.path.isdir(new_path):
    #             os.mkdir(new_path)
    #         self.metadata['Frames'].apply(rename)


class Center(Position):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.iteration = 1

    # def generate_report_png(self, defocus=True, save=True):
    #     File = self.stack.split('.')[0]
    #     cmap = mpl.cm.seismic
    #     fig = plt.figure(figsize=(10, 10), constrained_layout=True)
    #     spec = gridspec.GridSpec(ncols=9, nrows=8, figure=fig, wspace=0.1, hspace=2.5)

    #     if self.defocus:
    #         ax1 = fig.add_subplot(spec[0:2, 0:5])
    #         ax1.scatter(self.metadata.TiltAngle.round().astype('Int64'), self.metadata.df / 10000,
    #                     c=self.metadata.ctffit, cmap=mpl.cm.inferno, linewidths=1, edgecolors='black')
    #         ax1.set_title('Defocus')
    #         plt.xlabel('Tilt Angle (degrees)')
    #         plt.ylabel('Defocus(um)')
    #         ax4 = fig.add_subplot(spec[0:2, 5])
    #         norm = mpl.colors.Normalize(vmin=min(self.metadata.ctffit), vmax=max(self.metadata.ctffit))
    #         cb1 = mpl.colorbar.ColorbarBase(ax4, cmap=mpl.cm.inferno, norm=norm, orientation='vertical')
    #         plt.ylabel('CTF fit resolution (A)')
    #     ax2 = fig.add_subplot(spec[2:, 0:-1])

    #     x, y = [], []
    #     for i, j in self.metadata.sumxf:
    #         x.append(i * 1000)
    #         y.append(j * 1000)
    #     ax2.scatter(x, y, c=self.metadata.TiltAngle.round().astype('Int64'), cmap=cmap, linewidths=1, edgecolors='black')
    #     ax2.set_title('Tracking')
    #     ax2.set_xlabel('Movement in X (nm)')
    #     ax2.set_ylabel('Movement in Y (nm)')

    #     ax2.set_xlim([-self.metadata.ImageSize.iloc[-1][0] / 2 * self.metadata.PixelSpacing.iloc[-1] / 10,
    #                   self.metadata.ImageSize.iloc[-1][0] / 2 * self.metadata.PixelSpacing.iloc[-1] / 10])
    #     ax2.set_ylim([-self.metadata.ImageSize.iloc[-1][1] / 2 * self.metadata.PixelSpacing.iloc[-1] / 10,
    #                   self.metadata.ImageSize.iloc[-1][1] / 2 * self.metadata.PixelSpacing.iloc[-1] / 10])
    #     ax3 = fig.add_subplot(spec[2:, -1])
    #     norm = mpl.colors.Normalize(vmin=min(self.metadata.TiltAngle.round().astype('Int64')),
    #                                 vmax=max(self.metadata.TiltAngle.round().astype('Int64')))
    #     cb1 = mpl.colorbar.ColorbarBase(ax3, cmap=cmap, norm=norm, orientation='vertical')
    #     ax3.set_ylabel('Tilt Angle (degrees)')

    #     if save is True:
    #         plt.savefig(f'{File}.png')
    #         plt.close(f'{File}.png')
    #         plt.close(fig='all')

    def generate_analysis_file(self):
        with open(f'{self.analyzedFile}.lock', 'w') as file:
            file.write(f'{self.corr_x}\n')
            file.write(f'{self.corr_y}\n')
            if abs(self.dist) < self.threshold:
                file.write('2\n')

            else:
                file.write('1\n')
        shutil.move(f'{self.analyzedFile}.lock', self.analyzedFile)

        if abs(self.dist) < self.threshold:
            print('Centering below threshold...advancing to next step')
            if self.defocus:
                self.CTFfind()
            shutil.move(self.tempStack, self.stack)
            self.generate_report_png(defocus=self.defocus)
            self.iteration = 1
        else:
            print('Centering above threshold...Iterating')
            if self.iteration < 5:
                self.drop_meta_rows()
                os.remove(self.tempStack)
                self.iteration += 1
            else:
                self.iteration = 1
                shutil.move(self.tempStack, self.stack)
        self.remove_image()

    def logic(self):
        if self.check_if_image():
            if not os.path.isfile(self.stack):
                print(f'Initial creation of  {self.stack}')
                self.parse_mdoc()
                if self.defocus:
                    self.CTFfind()
                try:
                    shutil.copy(self.image, self.stack)
                except OSError:
                    time.sleep(2)
                    shutil.copy(self.image, self.stack)
                self.remove_image()
            else:
                self.parse_mdoc()
                self.calculate_correction()
                try:
                    self.generate_analysis_file()
                except OSError:
                    time.sleep(2)
                    self.generate_analysis_file()
                if int(round(self.metadata.TiltAngle.iloc[-1])) == self.tiltScheme[-1] and len(self.metadata.index) >= len(self.tiltScheme) and self.iteration == 1:
                    print('Finished')
                    self.finished = True
            self.save_metadata()


class Control(Position):
    def __init__(self, *args, **kwargs):
        for k, v in kwargs.items():
            if 'x' or 'y' in k:
                setattr(self, k, v)

        super().__init__(*args, **kwargs)

    def logic(self):
        pass


class CollectionArea(Position):
    def __init__(self, *args, **kwargs):
        for k, v in kwargs.items():
            if 'x' or 'y' or 'tiltScheme' in k:
                setattr(self, k, v)
        super().__init__(*args, **kwargs)

        if 'tiltScheme' in self.__dict__ and 'metadata' not in self.__dict__:
            self.refine_eucentric(initial=True)

        self.finished = False

    def logic(self):
        if self.check_if_image():
            if not os.path.isfile(self.stack):
                print(f'Initial creation of  {self.stack}')
                self.parse_mdoc().CTFfind()
                self.refine_eucentric(write_file=True)
                try:
                    shutil.copy(self.image, self.stack)
                    self.remove_image()
                except OSError:
                    time.sleep(2)
                    shutil.copy(self.image, self.stack)
                    self.remove_image()

            else:
                self.parse_mdoc().CTFfind()
                self.align_tilt(tempstack=False, iterate=2)
                self.remove_image()
                self.parse_xf()
                self.refine_eucentric(write_file=True)
                self.generate_report_png()

            try:
                self.metadata.at[len(self.metadata.index) - 1, 'SubFramePath'] = rename_frames(self.metadata.SubFramePath.iloc[-1],
                                                                                           f'{self.navI}_{self.name}_{str(self.metadata.iloc[-1].name).zfill(2)}_{int(round(self.metadata.iloc[-1].TiltAngle))}.tif')
            except Exception as err:
                print('Could not rename frames', err)

            if int(round(self.metadata.TiltAngle.iloc[-1])) == self.tiltScheme[-1] and len(self.metadata.index) >= len(self.tiltScheme):
                self.finished = True
                print('Done')
            self.save_metadata()
