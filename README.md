## Author information

The BISECT procedure was developed by the Borgnia and Bartesaghi laboratories.
* [Borgnia's Lab webpage](https://www.niehs.nih.gov/research/atniehs/facilities/mmc/index.cfm)
* [Bartesaghi's Lab webpage](http://cryoem.cs.duke.edu/)

## Citation
Bouvette, J., Liu, H.-F., Du, X., Zhou, Y., Sikkema, A. P., Da Fonseca, J., Klemm, B., Huang, R. K., Schaaper, R. M., Borgnia M. J., Bartesaghi, A. (2021). **Beam image-shift accelerated data acquisition for near-atomic resolution single-particle cryo-electron tomography.** Nature Communications,12,1957, https://doi.org/10.1038/s41467-021-22251-8
___
## Documentation

[Configuration](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home#Configuration)
* [Installation](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Installation)
* [Python package setup](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Configuration/Configuration-of-the-BISECT-python-package)
* [SerialEM setup](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Configuration/How-to-Configure-SerialEM-settings-for-BISECT)

[Usage](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/home#Usage)
* [Collecting regular BIS patterns](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Usage/Collecting-regular-holey-grid-patterns)
* [Collecting custom BIS patterns](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Usage/Collecting-custom-BIS-pattern)
* [Refining the BIS pattern](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Usage/Refining-the-BIS-pattern)
* [Saving hole reference images](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Usage/Hole-reference)
* [Providing custom tilt schemes](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Usage/Providing-customized-tilt-schemes)
* [Running bisectomo.py](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Usage/Running-the-script)

[Results/Outputs](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/home#resultsoutputs)
* [Directories](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Results/Directories)
* [Result Figures](https://gitlab.cs.duke.edu/bartesaghilab/bisect/-/wikis/Home/Results/Figures)

## Licensing

This work is licensed under the [BSD 3-Clause License](LICENSE)
